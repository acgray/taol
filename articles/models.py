from datetime import datetime
from bs4 import BeautifulSoup
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django_mongodb_engine.contrib import MongoDBManager
from djangotoolbox.fields import ListField, EmbeddedModelField, DictField
from haystack.query import SearchQuerySet
from mongom2m.fields import MongoDBManyToManyField
import time
from sorl.thumbnail.fields import ImageField
from admin.fields import TextListField
from articles.helpers.publication import PublicationMixin, PUB_STATUSES, PublishedManager, PUB_STATUS_CHOICES
from articles.stats.models import ArticlePageviewCounter
from images.models import Image
from taxonomy.models import Section
from uploads import gridfs_storage

class StringListField(ListField):
    def formfield(self, **kwargs):
        return TextListField(**kwargs)

class Comment(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    author = EmbeddedModelField('Author')
    text = models.TextField()

class Author(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)
    email = models.EmailField(blank=True,null=True)
    biog = models.TextField(blank=True,null=True)
    image = ImageField(upload_to='authors',blank=True)
    position = models.CharField(blank=True,max_length=255)
    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return 'author_detail',(self.slug,)

class ArticleImage(models.Model):
    image = models.ForeignKey(to=Image)
    caption = models.CharField(max_length=255)
    #format_overrides = DictField()
    is_main = models.BooleanField()

    def get_caption(self):
        return self.caption or self.image.description

class Article(PublicationMixin, models.Model):
    id = models.CharField(max_length=10, primary_key=True)
    title = models.CharField(max_length=500)
    slug = models.SlugField(max_length=100)
    deck = models.TextField(blank=True)
    author = MongoDBManyToManyField(to='Author',related_name='articles',blank=True)
    #TODO: replace with listfield - cleaner
    body = models.TextField(blank=True)
    images = ListField(EmbeddedModelField('ArticleImage'),blank=True)
    display_main_image = models.BooleanField(default=True)
    main_image_caption = models.CharField(max_length=255,null=True,blank=True)
    primary_section = models.ForeignKey(Section, to_field='slug', related_name='articles',null=True)
    keywords = StringListField(blank=True,null=True,help_text=(u'Use as many keywords as possible, comma separated, lowercase'))
    topics = MongoDBManyToManyField('taxonomy.Topic',embed=True,blank=True,null=True)
    related = ListField()

    objects = MongoDBManager()
    #objects = Manager()
    published = PublishedManager()

    created_by = models.ForeignKey(User,to_field='username',null=True,blank=True)
    created = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    modified = models.DateTimeField(auto_now=True,null=True,blank=True)
    pageviews = models.IntegerField(default=0)

    metadata = EmbeddedModelField(blank=True,null=True)
    comments_enabled = models.BooleanField(default=True)

    editors_pick = models.BooleanField(default=False)
    pin_on_section_page = models.BooleanField(default=False)
    pin_on_homepage = models.BooleanField(default=False)

    custom_template = models.CharField(blank=True,null=True,max_length=255)

    def track_pageview(self):
        ArticlePageviewCounter.objects.bump(self.id)
        Article.objects.raw_update({'_id':self.id},{'$inc':{'pageviews':1}})


    def save(self, force_insert=False, force_update=False, using=None):
        if not self.id:
            self.id = str(hex(int(time.time()*10000)))[2:]
        super(Article,self).save(force_insert, force_update, using)

    def delete_image(self,ai):
        #Article.objects.raw_update({"_id": ObjectId(self.id)}, {"$pull": {"images": {"image_id": ai.image_id}}})
        # TODO: make this work instead of this workaround
        self.images = [im for im in self.images if not im.image.id == ai.image_id]

        # Remove occurrences of image in body
        soup = BeautifulSoup(self.body)
        tags = soup.find_all('img',attrs={'data-image-id':ai.image_id})
        for t in tags:
            if len(t.parent.find_all()) == 1:
                t.parent.decompose()
            else:
                t.decompose()
        self.body=soup
        self.save()

    def add_image(self,image):
        self.images.append(ArticleImage(image=image))
        self.save()

    def set_main_image(self,ai):
        for i in range(0,len(self.images)):
            self.images[i].is_main=False
        self.images[ai].is_main = True
        self.save()

    @models.permalink
    def get_absolute_url(self):
        return ('article_detail',(),{
            'slug': self.slug,
            'pk': self.id,
            'sid': self.primary_section_id
        })

    @models.permalink
    def get_preview_url(self):
        return ('article_preview',(),{
            'pk': self.id
        })

    def get_main_image(self):
        for ai in self.images:
            if ai.is_main:
                return ai

    def get_display_image(self):
        try:
            return self.get_main_image() or  self.images[0]
        except IndexError:
            return None

    def get_related(self):
        #rem = 5 - len(self.related)
        if self.keywords:
            #auto_related = Article.published.raw_query({'keywords': {'$in': self.keywords}}).exclude(pk=self.pk)[:5]
            auto_related = Article.published.filter(keywords__in=self.keywords).exclude(pk=self.pk)[:5]
            return auto_related
        else:
            return []
            #sqs = SearchQuerySet().more_like_this(self).filter(pub_status='P').exclude(pk__in=self.related).exclude(pk=self.id)[:rem]
            #ids = self.related + [r.pk for r in sqs]
            #return Article.objects.filter(id__in=ids)

    def get_teaser(self):
        return self.deck or self.body

    def __unicode__(self):
        return unicode(self.title)

    def is_publishable_by(self,user):
        return user.has_perm('publish_article')


    @property
    def author_display(self):
        authors = [a.name for a in self.author.all()]
        print authors
        return ','.join(authors)

    class Meta:
        permissions = (
            ('publish_article','Publish article'),
        )
        ordering = ['-pub_date']
        get_latest_by = 'pub_date'

class ArticleWorkflowAction(models.Model):
    article = models.ForeignKey(to=Article,related_name='workflow_actions')
    old_status = models.CharField(max_length=1,choices=PUB_STATUS_CHOICES)
    new_status = models.CharField(max_length=1,choices=PUB_STATUS_CHOICES)
    user = models.ForeignKey(to=User)
    time = models.DateTimeField(auto_created=True)
    class Meta:
        get_latest_by = 'time'

class Metadata(models.Model):
    description = 'Metadata'
    class Meta:
        abstract=True

class SimpleMetadata(Metadata):
    pass

class ReviewMetadata(Metadata):

    description = "Review Metadata"

    rating = models.IntegerField(choices=(
        (1,1),
        (2,2),
        (3,3),
        (4,4),
        (5,5),
    ))

class SportMetadata(Metadata):

    description = "Sport Metadata"
    display_result = models.BooleanField(default=True)

    team1 = models.CharField(max_length=255)
    team1_result = models.CharField(max_length=50)
    team2 = models.CharField(max_length=255)
    team2_result = models.CharField(max_length=50)
