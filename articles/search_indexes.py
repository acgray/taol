from haystack import indexes
import datetime
from django.forms.fields import MultiValueField
from articles.models import Article

class ArticleIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    author = MultiValueField()
    pub_date = indexes.DateTimeField(model_attr='pub_date',null=True)
    pub_status = indexes.CharField(model_attr='pub_status')
    title_auto = indexes.EdgeNgramField(model_attr='title')
    keywords = indexes.MultiValueField(boost=5)

    def get_model(self):
        return Article

    def index_queryset(self):
        return self.get_model().objects.all()

    def prepare_author(self,obj):
        return [author.name for author in obj.authors.all]

    def prepare_keywords(self,obj):
        return obj.keywords or []
