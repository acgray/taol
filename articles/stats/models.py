from datetime import datetime
from django_mongodb_engine.contrib import MongoDBManager

from django.db import models

class ArticlePageviewCounterManager(MongoDBManager):
    def bump(self,article_id,sdate=None):
        # Have to set at runtime otherwise default is evaluated at compile time
        if not sdate:
            sdate = datetime.now().strftime("%y-%m-%d")
        self.raw_update({'article_id':article_id,'sdate':sdate},{'$inc':{'views':1}}, upsert=True)


class ArticlePageviewCounter(models.Model):
    article = models.ForeignKey(to='articles.Article', to_field='id')
    sdate = models.CharField(default=datetime.now().strftime("%y-%m-%d"),max_length=10)
    views = models.IntegerField()

    objects = ArticlePageviewCounterManager()

    #def __unicode__(self):
    #    return "%s: %s clicks" % (self.article.title, self.views)
    # TODO: fix properly
