from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache
from articles.models import Article

@never_cache
@csrf_exempt
def track_pageview_view(request):
    if request.POST:
        try:
            a = Article.objects.get(id=request.POST.get('article_id'))
            a.track_pageview()
        except Article.DoesNotExist:
            pass
    return HttpResponse()
