"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
import datetime
from bs4 import BeautifulSoup
from django.core.urlresolvers import reverse

from django.test import TestCase
from django.contrib.auth.models import User, Permission
from permission_backend_nonrel.utils import add_permission_to_user
import re
from sorl import thumbnail
from sorl.thumbnail.images import ImageFile
from articles.helpers.publication import PUB_STATUSES
from articles.models import Article, ArticleImage
from articles.templatetags.article_tags import body_filter
from images.models import Image
from django.core import files

from PIL import Image as PILImage

# TODO: refactor badly written tests

class ArticleModelTestCase(TestCase):
    def setUp(self):
        im = PILImage.new('RGB', (100,100), (0,0,0,0))
        im.save('/tmp/test.jpg', 'JPEG')
        self.article = Article.objects.create(title='Article', slug='article',pub_status='D')
        self.image = Image.objects.create(
            file = files.File(open('/tmp/test.jpg')),
            description = "This is my test image",
            credit = 'Adam Gray'
        )
        self.image2 = Image.objects.create(
            file = files.File(open('/tmp/test.jpg')),
            description = "A second test image",
            credit = 'Adam Gray'
        )
        self.article.images=[
            ArticleImage(
                image = self.image,
                is_main=True
            ),
            ArticleImage(
                image = self.image2,
            )
        ]
        self.article.save()
    def tearDown(self):
        ImageFile(self.image.file).delete()
        ImageFile(self.image2.file).delete()

    def test_get_main_image(self):
        self.assertIsInstance(self.article.get_main_image(),ArticleImage)
        self.assertTrue(self.article.get_main_image().is_main,True)

    def test_publish_permission_deny(self):
        #anonymous
        resp = self.client.get(reverse('admin:articles_article_publish',args=[self.article.id]))
        self.assertNotEqual(self.article.pub_status,'P')

        #logged in, w/o publish perm
        u=User.objects.create(username='editor',password='password')
        self.client.login(username='editor',password='password')
        resp = self.client.get(reverse('admin:articles_article_publish',args=[self.article.id]))
        self.assertNotEqual(self.article.pub_status,'P')

    def test_publish_permission_accept(self):
        #logged in w/ publish permission
        u=User.objects.create_user('editor','editor@theanglenews.co.uk','password')
        publish_article = Permission.objects.get(codename='publish_article')
        add_permission_to_user(publish_article,u)
        self.assertTrue(u.has_perm('articles.publish_article'))
        user = self.client.login(username='editor',password='password')
        resp = self.client.get(reverse('admin:articles_article_publish',args=[self.article.id]))

        # Need to requery article to update w/ changes
        self.article = Article.objects.get(id=self.article.id)
        self.assertEqual(self.article.pub_status,'P')

    def test_delete_image(self):
        self.article.body = """
        <p>Test article paragraph</p>
        <p><img src="mytestimage.jpg" data-image-id="%s" alt="My image caption" /></p>
        <p>Second article paragraph</p>
        <p><img src="mytestimage2.jpg" data-image-id="%s" alt="My second iamge caption" /></p>
        <p>Final paragraph</p>
        """ % (self.image.id, self.image2.id)
        self.article.save()
        ai = self.article.images[0]
        self.article.delete_image(ai)

        #only 1 image in article
        self.assertEqual(len(self.article.images),1)

        self.assertEqual(len(re.findall(r'<img',str(self.article.body))),1)

class PublicationMixinTestCase(TestCase):
    def setUp(self):
        self.article = Article.objects.create(
            title = 'Publish action test',
            slug = 'publish-action-test',
            pub_status=PUB_STATUSES['Draft']
        )

    def test_published_status(self):
        self.article = Article.objects.create(
            title = 'Test article',
            slug = 'test-article'
        )
        self.assertFalse(self.article.is_published)
        self.article.pub_status = 'P'
        self.article.save()
        self.assertTrue(self.article.is_published)

    def test_publish_method(self):
        self.article.publish()
        self.assertEqual(self.article.pub_status,PUB_STATUSES['Published'])

    def test_trash_method(self):
        self.article.trash()
        self.assertEqual(self.article.pub_status,'T')

class PublishedManagerTestCase(TestCase):
    def test_get_query_set(self):
        for n in range(1,10):
            a = Article.objects.create(
                title='My test article '+str(n),
                slug='my-test-article'+str(n),
                pub_date=datetime.datetime.now()
            )
            if not n % 2:
                a.pub_status=PUB_STATUSES['Published']
            else:
                a.pub_status=PUB_STATUSES['Draft']
            a.save()

        self.assertEqual(len(Article.published.all()),4)

class ArticleTemplateTagsTestCase(TestCase):
    def setUp(self):
        im = PILImage.new('RGB', (100,100), (0,0,0,0))
        im.save('/tmp/test2.jpg', 'JPEG')
        self.image = Image.objects.create(
            file = files.File(open('/tmp/test2.jpg')),
            description = 'A test image',
            credit = 'Adam Gray')
        self.article = Article.objects.create(
            title = 'My test article',
            slug = 'my-test-article',
            images = [
                ArticleImage(
                    image = self.image,
                    caption = 'My caption override',
                )
            ]
        )

    def tearDown(self):
        ImageFile(self.image.file).delete()
    def test_body_filters(self):
        self.article.body = """
                <p>My first paragraph</p>
                <p><img src="something.jpg" data-image-id="%s" alt="%s" data-image-caption="%s" /></p>
                <p>My second paragraph</p>""" % (self.image.id, 'My alt text', 'My article body caption override')
        self.article.save()
        filtered_body = body_filter(self.article.body)

        print filtered_body