# Create your views here.
from datetime import datetime
from bson.objectid import ObjectId
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import redirect
from django.template.defaultfilters import striptags
from django.views.generic.detail import DetailView
from haystack.views import SearchView
from articles.models import Article, Author


class BaseArticleDetailView(DetailView):
    model = Article

    def get_queryset(self):
        return Article.published.all()

    def get_context_data(self, **kwargs):
        context = super(BaseArticleDetailView,self).get_context_data(**kwargs)
        try:
            context['current_section'] = self.get_object().primary_section_id
        except AttributeError:
            context['current_section'] = ''
        return context

    def get_template_names(self):
        names = super(BaseArticleDetailView,self).get_template_names()
        if self.object.custom_template:
            names.insert(0,self.object.custom_template)
        return names

class ArticleDetailView(BaseArticleDetailView):


    def get(self, request, **kwargs):
        article = self.get_object()
        if not request.path == article.get_absolute_url():
            return redirect(article.get_absolute_url())
        return super(ArticleDetailView,self).get(request,**kwargs)

    def get_context_data(self,**kwargs):
        context = super(ArticleDetailView,self).get_context_data(**kwargs)
        context['page_title'] = striptags(self.object.title)
        return context


class ArticlePreviewView(BaseArticleDetailView):
    def get_queryset(self):
        return Article.objects.all()

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            raise Http404

        return super(ArticlePreviewView,self).dispatch(request,*args,**kwargs)

class AuthorDetailView(DetailView):
    model = Author
    template_name = 'articles/author_detail.html'

    def get_context_data(self, **kwargs):
        context = super(AuthorDetailView,self).get_context_data(**kwargs)
        context['author_published_articles'] = Article.objects.raw_query({'pub_status':'P', 'pub_date': {'$lte': datetime.now()}, 'author.id': ObjectId(self.object.id)})
        return context
