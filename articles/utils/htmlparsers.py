from bs4 import BeautifulSoup

def parse_images(html):
    soup = BeautifulSoup(html)
    images = soup.find_all('img',attrs={'data-img':True})

