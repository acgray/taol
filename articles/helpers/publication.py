from datetime import datetime
from django.db import models
from django_mongodb_engine.contrib import MongoDBManager

PUB_STATUS_CHOICES = (
    ('D', 'Draft'),
    ('E', 'Edit'),
    ('S', 'Subedit'),
    ('A', 'Waiting approval'),
    ('P', 'Published'),
    ('T', 'Trash'),
)

PUB_STATUSES = dict((pair[1], pair[0]) for pair in PUB_STATUS_CHOICES)

class PublicationMixin(models.Model):
    pub_date = models.DateTimeField(blank=True,null=True, default=datetime.now(),help_text='Set in the past to publish immediately')
    pub_status = models.CharField((u'Publication status'), max_length=1,
                                  choices=PUB_STATUS_CHOICES, default='D', help_text=(
            u'Only published items will appear on the site'))

    def publish(self):
        self.pub_status=PUB_STATUSES['Published']
        if not self.pub_date:
            self.pub_date = datetime.now()
        self.save()

    def trash(self):
        self.pub_status='T'
        self.save()


    @property
    def is_published(self):
        return self.pub_status == PUB_STATUSES['Published']

    def get_pub_status_desc(self):
        if self.pub_date:
            if self.is_published and self.pub_date > datetime.now():
                return 'Scheduled for %s' % self.pub_date.strftime('%d %h %Y, %H:%M')
        return self.get_pub_status_display()

    class Meta:
        abstract = True

class PublishedManager(MongoDBManager):
    def get_query_set(self):
        return super(PublishedManager, self).get_query_set()\
            .filter(pub_date__lte=datetime.now())\
            .filter(pub_status="P")
