from datetime import datetime, date as date_cls
from bs4 import BeautifulSoup
from django import template
from django.conf import settings
from django.template import defaultfilters
from django.template.defaultfilters import stringfilter
from django.template.loader import render_to_string
from django.utils import formats
from django.utils.dateformat import format
from django.utils.translation import ungettext, pgettext
from images.models import Image
from django.utils.translation import ugettext as _

register = template.Library()

@register.filter
@stringfilter
def body_filter(value):
    soup = BeautifulSoup(value)

    images = soup.find_all('img',attrs={'data-image-id':True})
    for i in images:
        im = Image.objects.get(id=i.attrs['data-image-id'])
        try:
            caption = i.attrs['data-image-caption']
        except KeyError:
            caption = im.description


        imgcode = render_to_string('articles/components/image.html',{
            'caption': caption,
            'credit': im.credit,
            'image': im,
        })

        i.replace_with(BeautifulSoup(imgcode))

    return soup

class ArticleMetadataNode(template.Node):
    def __init__(self, metadata, name=None):
        self.metadata = template.Variable(metadata)
        if name:
            self.name = template.Variable(name)

    def render(self, context):
        try:
            actual_metadata = self.metadata.resolve(context)
            try:
                name = self.name.resolve(context)
            except AttributeError:
                pass
        except template.VariableDoesNotExist:
            return ''
        if not actual_metadata:
            return ''
        template_name = actual_metadata._meta.verbose_name.replace(' ','_')
        try:
            template_path = 'articles/metadata/%s/%s.html' % (name,template_name)
        except NameError:
            template_path = 'articles/metadata/%s.html' % template_name
        return render_to_string(template_path, {
            'metadata': actual_metadata
        })

@register.tag
def render_metadata(parser,token):
    bits = token.split_contents()[1:]
    if len(bits) < 1:
        raise template.TemplateSyntaxError("%r tag requires at least one argument")
    if len(bits) == 1:
        metadata = bits [0]
        return ArticleMetadataNode(metadata)
    if len(bits) == 2:
        metadata = bits[0]
        name = bits [1]
        return ArticleMetadataNode(metadata,name)
    else:
        raise template.TemplateSyntaxError("%r tag requires max 2 arguments")

@register.filter
def naturaltime(value):
    """
    For date and time values shows how many seconds, minutes or hours ago
    compared to current timestamp returns representing string.
    """
    if not isinstance(value, date_cls): # datetime is a subclass of date
        return value

    now = datetime.now()

    if value < now:
        delta = now - value
        if delta.days > 2:
            return value
        if delta.days != 0:
            return pgettext(
                'naturaltime', '%(delta)s ago'
            ) % {'delta': defaultfilters.timesince(value, now)}
        elif delta.seconds == 0:
            return _('now')
        elif delta.seconds < 60:
            return ungettext(
                'a second ago', '%(count)s seconds ago', delta.seconds
            ) % {'count': delta.seconds}
        elif delta.seconds // 60 < 60:
            count = delta.seconds // 60
            return ungettext(
                'a minute ago', '%(count)s minutes ago', count
            ) % {'count': count}
        else:
            count = delta.seconds // 60 // 60
            return ungettext(
                'an hour ago', '%(count)s hours ago', count
            ) % {'count': count}
    else:
        delta = value - now
        if delta.days != 0:
            return pgettext(
                'naturaltime', '%(delta)s from now'
            ) % {'delta': defaultfilters.timeuntil(value, now)}
        elif delta.seconds == 0:
            return _('now')
        elif delta.seconds < 60:
            return ungettext(
                'a second from now', '%(count)s seconds from now', delta.seconds
            ) % {'count': delta.seconds}
        elif delta.seconds // 60 < 60:
            count = delta.seconds // 60
            return ungettext(
                'a minute from now', '%(count)s minutes from now', count
            ) % {'count': count}
        else:
            count = delta.seconds // 60 // 60
            return ungettext(
                'an hour from now', '%(count)s hours from now', count
            ) % {'count': count}

@register.filter
def date(value, arg=None):
    """Formats a date according to the given format."""
    if not value:
        return ''
    if arg is None:
        arg = settings.DATE_FORMAT
    try:
        return formats.date_format(value, arg)
    except AttributeError:
        try:
            return format(value, arg)
        except AttributeError:
            return ''