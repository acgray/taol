from datetime import datetime,timedelta
from django import template
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from articles.models import Article
from articles.stats.models import ArticlePageviewCounter
from layout.utils import render_model
from taxonomy.models import Section

register = template.Library()

@register.inclusion_tag('blocks/top_content.html')
def top_content_block():
    most_recent = Article.published.order_by('-pub_date')[:5]

    #TODO: put this somewhere better
    #yest = datetime.now()-timedelta(days=1)
    #dates = [datetime.now().strftime("%y-%m-%d"),yest.strftime("%y-%m-%d")]
    #values = ArticlePageviewCounter.objects.filter(sdate__in=dates).order_by('-views')[:5]
    values = ArticlePageviewCounter.objects.filter(sdate=datetime.now().strftime("%y-%m-%d")).order_by('-views')[:5]
    most_read = [x.article for x in values]
    return {
        'most_recent': most_recent,
        'most_commented': [],
        'most_read': most_read,
        # TODO: implement most_commented and most_popular properly
    }

class SectionBlockNode(template.Node):
    def __init__(self,slug,name):
        try:
            self.section = Section.objects.get(slug=slug)
        except Section.DoesNotExist:
            self.section = None
        self.name = template.Variable(name)

    def render(self, context):
        if not self.section:
            return ''
        name = self.name.resolve(context)
        context['articles'] = Article.published.filter(primary_section=self.section).exclude(pin_on_homepage=True)[:4]
        return render_model(self.section,name,context_instance=context)

@register.tag
def section_block(parser,token):
    try:
        _, slug, name = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r takes exactly 2 arguments")

    return SectionBlockNode(slug,name)

class EditorsPickBlockNode(template.Node):
    def __init__(self,excl_slug):
        self.excl_slug = template.Variable(excl_slug)
    def render(self, context):
        excl_slug = self.excl_slug.resolve(context)
        article_list = Article.objects.raw_query({'primary_section_id': {'$ne': excl_slug},'pub_status':'P','pub_date': {'$lte':datetime.now()}}).filter(editors_pick=True)[:10]
        #article_list = []
        context = {
            'article_list': article_list
        }
        return mark_safe(render_to_string('blocks/editors_pick.html',context))

@register.tag
def editors_pick(parser,token):
    try:
        _, excl_slug = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("invalid syntax")

    return EditorsPickBlockNode(excl_slug)

