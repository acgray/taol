from mimetypes import guess_type
from django.http import Http404, HttpResponse
from gridfs.errors import NoFile
from taol import settings
from uploads.gridfs_storage import gridfs_storage

__author__ = 'acgray'


if settings.DEBUG:
    def serve_from_gridfs(request,path):
        try:
            gridfile = gridfs_storage.open(path)
        except NoFile:
            raise Http404
        else:
            return HttpResponse(gridfile, mimetype=guess_type(path)[0])