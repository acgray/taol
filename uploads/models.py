from django.db import models
from uploads.gridfs_storage import gridfs_storage

__author__ = 'acgray'

class FileUpload(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    file = models.FileField(storage=gridfs_storage, upload_to='/')
