# The Angle

Primary CMS for The Angle Online.

## Requirements/dependencies

* MongoDB
* Xapian
* LDAP (optional)

## Author

Adam Gray <info@adamgray.me.uk>

## License

THiS IS PROPRIETARY SOFTWARE.  THE SOURCE CODE IS PROVIDED FOR INFORMATION ONLY AND MAY DISAPPEAR AT ANY TIME.