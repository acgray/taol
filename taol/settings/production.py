from .default import *

DEBUG = False
STATIC_ROOT = '/srv/webapps/theangle/static'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/var/log/gunicorn/taol.django.log',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['console','mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

CACHE_MIDDLEWARE_ALIAS = 'taol'
CACHE_MIDDLEWARE_SECONDS = 60
CACHE_MIDDLEWARE_KEY_PREFIX = ''

THUMBNAIL_DEBUG = False

FACEBOOK_APP_ID = '130973980331487'
FACEBOOK_APP_SECRET = '7346c4579b12da0b459662a6223a9cc8'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'