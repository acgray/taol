$(function() {
    $('#top_content_tabs').tabs({selected: 1});

    $('.flexslider').flexslider();

    if ($('#admin_toolbar').length > 0) {
        $('body').addClass('admin-toolbar-enabled');
    }
});

