from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.conf import settings

def facebook_channel_view(request):
    return HttpResponse('<script src="//connect.facebook.net/en_US/all.js"></script>')

def example(request):
    return render_to_response('social/example.html',{'facebook_app_id': settings.FACEBOOK_APP_ID})