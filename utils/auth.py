from django.contrib.auth.models import Group
from django.http import HttpResponseForbidden
from permission_backend_nonrel.models import UserPermissionList

def user_in_group(User, groupName):
    group = Group.objects.get(name=groupName)
    up = UserPermissionList.objects.filter(user = User)
    try:
        return True if unicode(group.id) in up[0].group_fk_list else False
    except:
        return False

def process_wf_action(request, article, wf_target):
    if wf_target == 'P':
        if not request.user.has_perm('articles.publish_article'):
            raise Exception('Insufficiant permission')
        article.publish()

    if wf_target == 'S':
        article.pub_status = 'S'
        article.save()

    if wf_target == 'A':
        if not user_in_group(request.user,'subeditors'):
            return HttpResponseForbidden('You are not allowed to do that')
        article.pub_status = 'A'
        article.save()

    if wf_target == 'D':
        article.pub_status = 'D'
        article.save()

    if wf_target == 'E':
        article.pub_status = 'E'
        article.save()