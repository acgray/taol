from exceptions import ValueError
from django.core.paginator import Paginator, EmptyPage, InvalidPage

def paginate(image_list, request, per_page=10):
    paginator = Paginator(image_list, per_page)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        images = paginator.page(page)
    except (EmptyPage, InvalidPage):
        images = paginator.page(paginator.num_pages)
    return images