from datetime import datetime
from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView
from haystack.forms import SearchForm
from haystack.query import SearchQuerySet
from haystack.views import search_view_factory, SearchView
from articles.models import Author, Article
from articles.views import ArticleDetailView, ArticlePreviewView, AuthorDetailView
from images.models import Image
from layout.views import  QuerySetBackedWellView
from taol import settings
from taxonomy.views import DefaultSectionView, NewsSectionView

admin.autodiscover()
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from uploads.views import serve_from_gridfs



urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'taol.views.home', name='home'),
    # url(r'^taol/', include('taol.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    #url(r'^admin/', include(admin.site.urls)),

    url(r'^images/create$', CreateView.as_view(model=Image), name='image_create'),
    url(r'^images/(?P<pk>[a-z\d]+)', DetailView.as_view(model=Image), name='image_detail'),
    url(r'^images$', ListView.as_view(model=Image), name='image_list'),

    url(r'uploads/(?P<path>.+)', serve_from_gridfs),

    url(r'^ta_admin/', include('admin.urls', namespace='admin', app_name='admin')),
    (r'^accounts/login/$', 'django.contrib.auth.views.login'),
    url(r'^accounts/logout$', 'django.contrib.auth.views.logout_then_login', name='logout'),

    # SEARCH
    #url(r'^search/', include('haystack.urls',namespace='haystack')),

    # FACEBOOK
    url(r'^channel$', 'social.views.facebook_channel_view', name='facebook_channel'),
    url(r'^fbexample$', 'social.views.example'),

    # FRONTEND

    url(r'^$', QuerySetBackedWellView.as_view(well_title='homepage',template_name='layout/wells/homepage.html',queryset=Article.published.filter(pin_on_homepage=True))),
    url(r'^section/news$', DefaultSectionView.as_view(well_title='news',template_name='layout/wells/news.html'), kwargs={'slug': 'news'}),
    #url(r'^section/features$', DefaultSectionView.as_view(well_title='features',template_name='layout/wells/features.html'), kwargs={'slug': 'features'}),
    url(r'^section/features$', DefaultSectionView.as_view(template_name='layout/wells/features.html'), kwargs={'slug': 'features'}),
    #url(r'^section/comment', DefaultSectionView.as_view(template_name='layout/wells/comment.html'), kwargs={'slug': 'comment'}),
    url(r'^section/(?P<slug>[a-z\-]+)$', DefaultSectionView.as_view(), name='section_detail'),

    url(r'^article/(?P<pk>[a-f\d]+)$', ArticlePreviewView.as_view(), name='article_preview'),
    url(r'^(?P<sid>[a-z\d\-]+)/article-(?P<pk>[a-f\d]+)/(?P<slug>[a-z\-\d]+).html$', ArticleDetailView.as_view(), name='article_detail'),
    #track article pageview
    url(r'^stats/track$', 'articles.stats.views.track_pageview_view', name='article_track_pageview'),
    url(r'^author/(?P<slug>[a-z\-]+).html$', AuthorDetailView.as_view(), name='author_detail'),


    # STATIC PAGES
    url(r'^pages/advertising.html$', TemplateView.as_view(template_name='staticpages/advertising.html')),
    url(r'^pages/contact.html$', TemplateView.as_view(template_name='staticpages/contact.html')),
    url(r'^pages/about.html$', TemplateView.as_view(template_name='staticpages/about.html')),

    # SECTION REDIRECTS
    url(r'^news$', lambda x: HttpResponseRedirect('/section/news')),
    url(r'^comment$', lambda x: HttpResponseRedirect('/section/comment')),
    url(r'^features$', lambda x: HttpResponseRedirect('/section/features')),
    url(r'^culture$', lambda x: HttpResponseRedirect('/section/culture')),
    url(r'^fashion', lambda x: HttpResponseRedirect('/section/fashion')),
    url(r'^sport$', lambda x: HttpResponseRedirect('/section/sport')),

    # ROBOTS
    url(r'robots\.txt$', lambda r: HttpResponse("User-agent: *\nAllow: /", mimetype="text/plain")),
)

sqs = SearchQuerySet().filter_and(pub_status='P',pub_date__lte=datetime.now())
urlpatterns += patterns('haystack.views',
                       url(r'^search/$', search_view_factory(
                           view_class=SearchView,
                           searchqueryset=sqs,
                           form_class=SearchForm
                       ), name='haystack_search'),
                       )

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
            }),
    )
