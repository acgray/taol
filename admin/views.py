# Create your views here.
from datetime import datetime
from braces.views import LoginRequiredMixin
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Group
from django.core import mail
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from django.core.urlresolvers import reverse
from django.db.models.query import QuerySet
from django.db.models.query_utils import Q
from django.forms.formsets import formset_factory
from django.forms.models import  modelform_factory, inlineformset_factory
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.template.context import RequestContext
from django.template.defaultfilters import slugify
from django.utils.datastructures import MultiValueDictKeyError
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.views.generic.list import ListView
from django_tables2.config import RequestConfig
from admin.forms.article import AdminArticleForm, AdminArticleCreateForm, AdminArticleSearchForm
from admin.forms.images_forms import ImageForm, AdminImageSearchForm
from admin.forms.layout_forms import AdminWellForm, AdminWellCreateForm, AdminArticleListForm
from admin.forms.taxonomy_forms import SectionForm
from admin.tables import ArticleTable, ImageTable, AuthorTable, SectionTable, ArticleListTable
import articles
from articles.helpers.publication import PUB_STATUSES
from articles.models import Article, ArticleImage, Author, ArticleWorkflowAction
from images.models import Image
from layout.models import Well, ArticleList, Node

#####GENERIC ADMIN VIEWS

# TODO: refactor with django-braces PermissionRequired mixin
from taxonomy.models import Section
from utils.auth import user_in_group
from utils.helpers import paginate

class AdminDynamicContextMixin(object):
    def get_context_data(self, **kwargs):
        context = super(AdminDynamicContextMixin,self).get_context_data(**kwargs)
        qs = super(AdminDynamicContextMixin,self).get_queryset()
        context['object_name'] = qs.model._meta.verbose_name
        context['object_name_plural'] = qs.model._meta.verbose_name_plural
        context['create_url'] = reverse('admin:'+qs.model._meta.app_label+'_'+qs.model._meta.object_name.lower()+'_create')
        return context

class AdminListView(AdminDynamicContextMixin, ListView):
    def get_context_data(self, **kwargs):
        context = super(AdminListView,self).get_context_data(**kwargs)
        try:
            table_class = self.table_class
        except AttributeError:
            return context
        table = table_class(self.get_queryset())
        RequestConfig(self.request,paginate={'per_page': 10}).configure(table)
        context['%s_table' % self.object_list.model._meta.object_name.lower()] = table
        context['object_table'] = table
        return context

    def get_template_names(self):
        names = ['admin/default_list.html']
        names.insert(0, 'admin/%s_%s_list.html' % (self.object_list.model._meta.app_label, self.object_list.model._meta.object_name.lower(),))
        return names

class AdminCreateView(AdminDynamicContextMixin, CreateView):
    template_name='admin/default_form.html'
    def get_success_url(self):
        try:
            return self.request.GET['success']
        except KeyError:
            return reverse("admin:%s_%s_changelist" % (self.object._meta.app_label, self.object._meta.object_name.lower()))

class AdminUpdateView(AdminDynamicContextMixin, UpdateView):
    template_name = 'admin/default_form.html'
    def get_success_url(self):
        return reverse("admin:%s_%s_changelist" % (self.object._meta.app_label, self.object._meta.object_name.lower()))


@login_required
def admin_homepage_view(request):
    context = {}
    try:
        if user_in_group(request.user, 'editors'):
            context['intray_title'] = 'My intray (editor)'
            qs = Article.objects.filter(Q(pub_status='E')|Q(pub_status='A'))
        elif user_in_group(request.user, 'subeditors'):
            context['intray_title'] = 'My intray (subeditor)'
            qs = Article.objects.filter(pub_status='S')
            print qs
        else:
            qs = None
        context['has_intray'] = True
    except Group.DoesNotExist:
        qs = None
    if qs:
        table = ArticleTable(qs)
        RequestConfig(request).configure(table)
        context['intray_table'] = table

    context['recent_actions'] = ArticleWorkflowAction.objects.all().order_by('-time')[:10]
    return render_to_response('admin/homepage.html',context)


# TODO: refactor to inherit from AdminListView
class AdminArticleListView(LoginRequiredMixin, ListView):
    model = Article
    paginate_by = 5
    ordering= ['-id']

    def get_template_names(self):
        names = super(AdminArticleListView,self).get_template_names()
        names.insert(0,'admin/article_list.html')
        return names

    def get_queryset(self):
        qs = super(AdminArticleListView,self).get_queryset().exclude(pub_status='T').order_by('-id')
        self.search_form = AdminArticleSearchForm(self.request.GET)
        if self.request.GET.get('filter'):
            return qs.filter(title__icontains=self.request.GET.get('filter'))
        return qs

    def get_context_data(self, **kwargs):
        context = super(AdminArticleListView, self).get_context_data(**kwargs)
        table = ArticleTable(self.get_queryset())
        RequestConfig(self.request, paginate={"per_page": 20}).configure(table)
        context['article_table'] = table

        my_articles = Article.objects.filter(created_by=self.request.user.username).exclude(pub_status='P').exclude(
            pub_status='T')
        context['my_articles_list'] = my_articles
        context['my_articles_table'] = ArticleTable(my_articles)
        context['article_search_form'] = self.search_form
        return context

class AdminArticleCreateView(LoginRequiredMixin,CreateView):
    model = Article
    form_class = AdminArticleCreateForm
    template_name = 'admin/article_create.html'

    def get_success_url(self):
        return reverse('admin:articles_article_change',args=(self.object.id,))

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.slug = slugify(form.instance.title)
        type = self.request.POST['type']
        if not type == 'simple':
            metadata_cls_name = type.title()+'Metadata'
            metadata_cls = getattr(articles.models, metadata_cls_name)
            form.instance.metadata= metadata_cls()
        return super(AdminArticleCreateView, self).form_valid(form)

class AdminArticleDeleteView(LoginRequiredMixin,DeleteView):
    model = Article
    template_name = 'admin/articles_article_confirm_delete.html'
    def get_success_url(self):
        return reverse('admin:articles_article_changelist')

class AdminArticleImageInsertView(LoginRequiredMixin,ListView):
    model = ArticleImage

    def get_queryset(self, **kwargs):
        qs = super(AdminArticleImageInsertView,self).get_queryset()
        return qs

    def get(self, request, *args, **kwargs):
        self.object_list = self.get(**kwargs)
        context = self.get_context_data(object_list=self.object_list)
        return self.render_to_response(context)

def admin_articleimage_insert_view(request,**kwargs):
    article = Article.objects.get(id=kwargs['pk'])
    context = {'images': article.images}
    return render_to_response('admin/article_articleimage_browse.html',context)

class AdminImageListView(LoginRequiredMixin,ListView):
    model = Image
    template_name = 'admin/image_list.html'
    form_class = ImageForm

    def get_queryset(self):
        self.search_form = AdminImageSearchForm(self.request.GET)
        if self.request.GET.get('filter'):
            return Image.objects.filter(description__icontains=self.request.GET.get('filter'))
        return Image.objects.all()

    def get_context_data(self, **kwargs):
        context = super(AdminImageListView,self).get_context_data(**kwargs)
        table = ImageTable(self.get_queryset())
        RequestConfig(self.request, paginate={'per_page':5}).configure(table)
        context['image_table'] = table
        context['image_search_form'] = self.search_form
        return context

class AdminImageUpdateView(LoginRequiredMixin,UpdateView):
    model = Image
    template_name = 'admin/image_form.html'

    def get_context_data(self, **kwargs):
        context = super(AdminImageUpdateView,self).get_context_data(**kwargs)
        object = self.get_object()
        context['articles_used_list'] = Article.objects.raw_query({'images.image_id':object.id})
        return context

    def get_success_url(self):
        return reverse('admin:images_image_changelist')

class AdminImageCreateView(LoginRequiredMixin,AdminCreateView):
    model = Image

    def get_template_names(self):
        if self.request.GET.get('success'):
            return 'admin/image_form_mini.html'
        return 'admin/image_form.html'

class AdminArticleUpdateView(LoginRequiredMixin,UpdateView):
    model = Article
    template_name = 'admin/article_form.html'
    form_class = AdminArticleForm

    def get_form_kwargs(self):
        kwargs = super(AdminArticleUpdateView,self).get_form_kwargs()
        kwargs['request_user'] = self.request.user
        return kwargs

    def post(self, request, *args, **kwargs):
        try:
            wf_target = request.POST['wf_target']
        except MultiValueDictKeyError:
            return super(AdminArticleUpdateView,self).post(request, *args, **kwargs)
        article = self.get_object()

        class PermissionDeniedException(Exception):
            pass

        def record_wf_action(wf_target,article=article,request=request):
            old_status = article.pub_status
            new_status = wf_target
            ArticleWorkflowAction.objects.create(user=request.user,article=article,old_status=old_status,new_status=new_status,time=datetime.now())
            article.pub_status = wf_target
            notification = mail.EmailMessage(from_email='autonotify@theanglenews.co.uk')
            PUB_STATUSES_VALUES = dict(zip(PUB_STATUSES.values(),PUB_STATUSES.keys()))

            notification.subject = '**%s: %s' % (PUB_STATUSES_VALUES[wf_target],article.title)
            if wf_target == 'E' and old_status == 'D':
                notification.to = ['senior-editors@theanglenews.co.uk']
            elif wf_target == 'A' and old_status == 'S':
                notification.to = ['editor@theanglenews.co.uk']
            elif wf_target == 'S':
                notification.to = ['subeditors@theanglenews.co.uk']

            notification.body = 'User %s has just changed article "%s" from %s to %s. \n\nPlease modify the article as necessary by visiting %s\n\n(This is an automatically generated email)' % (request.user, article.title, PUB_STATUSES_VALUES[old_status], PUB_STATUSES_VALUES[new_status], request.build_absolute_uri(reverse('admin:articles_article_change',args=[article.id])))
            notification.extra_headers = {'Reply-To': 'webmaster@theanglenews.co.uk'}
            notification.send()
        try:
            if article.pub_status == 'P':
                if not request.user.has_perm('articles.publish_article'):
                    raise PermissionDeniedException

            if wf_target == 'P':
                if not request.user.has_perm('articles.publish_article'):
                    raise PermissionDeniedException
                record_wf_action('P')
                article.publish()

            if wf_target == 'S':
                record_wf_action('S')

            if wf_target == 'A':
                if not (user_in_group(request.user,'subeditors') or user_in_group(request.user,'editors')):
                    raise PermissionDeniedException
                record_wf_action('A')

            if wf_target == 'D':
                record_wf_action('D')

            if wf_target == 'E':
                record_wf_action('E')

            if wf_target == 'T':
                record_wf_action('T')

            article.save()
            messages.add_message(request, messages.SUCCESS, 'Article status is now %s' % article.get_pub_status_display())


        except PermissionDeniedException:
            messages.add_message(request, messages.ERROR, 'You are not allowed to do that')

        return HttpResponseRedirect(reverse('admin:articles_article_change', args=[article.pk]))


    def get_success_url(self):
        if self.request.POST.get('action','save') == 'save_and_close':
            return reverse('admin:articles_article_changelist')
        return reverse('admin:articles_article_change',args=[self.object.pk])

    def get_context_data(self, **kwargs):
        context = super(AdminArticleUpdateView,self).get_context_data(**kwargs)
        context['user_is_subeditor'] = user_in_group(self.request.user,'subeditors')
        context['user_is_editor'] = user_in_group(self.request.user, 'editors')
        context['related_articles_list'] = self.object.get_related()
        context['latest_actions'] = ArticleWorkflowAction.objects.filter(article=self.object).order_by('-time')[:5]
        if self.object.metadata:
            metadata_form = modelform_factory(self.object.metadata.__class__)
            if self.request.POST:
                context['articlemetadata_form'] = metadata_form(self.request.POST)
            else:
                context['articlemetadata_form'] = metadata_form(instance=self.object.metadata)
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        try:
            metadata_form = context['articlemetadata_form']
        except KeyError:
            messages.add_message(self.request, messages.SUCCESS, 'Article has been saved')
            return super(AdminArticleUpdateView,self).form_valid(form)
        if metadata_form.is_valid():
            self.object = form.save()
            self.object.metadata = metadata_form.instance
            self.object.save()
            messages.add_message(self.request,messages.SUCCESS,'Article has been successfully saved')
            return redirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form))

    def form_invalid(self, form):
        messages.add_message(self.request,messages.ERROR, 'There were some problems with the form and the article was not saved. Please check below and try again.')
        return super(AdminArticleUpdateView,self).form_invalid(form)

@login_required
def admin_article_mark_ready(request,**kwargs):
    article = Article.objects.get(id=kwargs['pk'])
    article.pub_status = 'E'
    return HttpResponse('Article status is now '+article.get_pub_status_display())

@login_required
def admin_articleimage_manage_view(request,**kwargs):
    article = Article.objects.get(id=kwargs.get('pk'))
    if request.POST.get('image_id'):
        if not Article.objects.raw_query({'_id': article.id, 'images.image_id':request.POST.get('image_id')}):
            #article.add_image(image)
            article.images.append(ArticleImage(
                image_id=request.POST.get('image_id')
            ))
            article.save()
            #return redirect(reverse('admin:articles_article_change', args=(article.id,)))
    #else:
    elif request.POST.get('delete_image'):
        article.delete_image(article.images[int(request.POST.get('delete_image'))])
    elif request.POST.get('main_image[]'):
        print "main image: %s" % request.POST.get('main_image[]')
        article.set_main_image(int(request.POST.get('main_image[]')))

    image_list = Image.objects.all().order_by('-created_on')

    images = paginate(image_list, request, 5)

    context = RequestContext(request)
    context['images'] = images
    context['article'] = article
    return render_to_response('admin/articleimage_manage.html',context)

@login_required
def admin_article_related_manage_view(request,**kwargs):
    if request.POST.get('related_article_ids[]'):
        aid = kwargs.get('pk')
        Article.objects.filter(id=aid).update(related=request.POST.getlist('related_article_ids[]'))
        return HttpResponse('done')
    article = Article.objects.get(id=kwargs.get('pk'))
    paginator = Paginator(Article.objects.all(), 5)
    page = request.GET.get('page',1)
    try:
        article_list = paginator.page(page)
    except (EmptyPage, InvalidPage):
        article_list = paginator.page(paginator.num_pages)
    related_list = Article.objects.filter(id__in=article.related)
    context = RequestContext(request)
    context['article'] = article
    context['articles'] = article_list
    context['related_list'] = related_list
    return render_to_response('admin/article_related_manage.html',context)

@login_required
def admin_article_metadata_manage_view(request,**kwargs):
    article = Article.objects.get(id=kwargs.get('pk'))
    if not article.metadata:
        return HttpResponse("This article has no metadata")

    metadata_model = article.metadata.__class__
    Form = modelform_factory(model=metadata_model)

    return render_to_response("admin/article_metadata_manage.html", {
        'form': Form(instance=article.metadata)
    })


### AUTHORS
class AdminAuthorListView(LoginRequiredMixin,ListView):
    template_name = 'admin/articles_author_list.html'

    def get_queryset(self):
        return Author.objects.all().order_by('name')

    def get_context_data(self, **kwargs):
        context = super(AdminAuthorListView,self).get_context_data(**kwargs)
        table = AuthorTable(self.get_queryset())
        RequestConfig(self.request,paginate={'per_page': 10}).configure(table)
        context['author_table'] = table
        return context

### WELLS

class AdminWellUpdateView(LoginRequiredMixin, UpdateView):
    model = Well
    template_name = 'admin/layout_well_form.html'
    form_class = AdminWellForm

    def get_context_data(self, **kwargs):
        context = super(AdminWellUpdateView,self).get_context_data(**kwargs)
        object = self.get_object()
        ids = []
        for n in object.nodes:
            ids.append(n.content_object.id)
        context['articles'] = Article.objects.all().exclude(id__in=ids)
        return context

    def get_success_url(self):
        return reverse('admin:layout_well_change',args=(self.get_object().id,))

class AdminWellCreateView(LoginRequiredMixin, AdminCreateView):
    model = Well
    form_class = AdminWellCreateForm

    def get_success_url(self):
        return reverse('admin:layout_well_changelist')

class AdminArticleListListView(LoginRequiredMixin, AdminListView):
    model = ArticleList
    table_class = ArticleListTable
    def get_context_data(self, **kwargs):
        context = super(AdminArticleListListView,self).get_context_data(**kwargs)
        table = ArticleListTable(self.get_queryset())
        RequestConfig(self.request, paginate={"per_page": 10}).configure(table)
        context['object_table'] = table
        return context

class AdminArticleListUpdateView(LoginRequiredMixin, AdminUpdateView):
    model = ArticleList
    form_class = AdminArticleListForm
    template_name = 'admin/layout_articlelist_form.html'

    def get_formset_class(self):
        return formset_factory(modelform_factory(Node), can_delete=True, extra=1)

    def get_context_data(self, **kwargs):
        node_dict = [{'content_object': n.content_object,'title_override':n.title_override} for n in self.object.nodes]
        self.formset_class = self.get_formset_class()
        self.formset = kwargs.get('formset') or self.formset_class(initial=node_dict)
        context = super(AdminArticleListUpdateView,self).get_context_data(**kwargs)
        context['node_formset'] = self.formset
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset_class = self.get_formset_class()
        formset = formset_class(request.POST)
        if form.is_valid() and formset.is_valid():
            nodes = []
            for f in formset.forms:
                node = f.instance
                print f.fields['DELETE'].clean
                if node.content_object and not f._raw_value('DELETE'):
                    nodes.append(node)
            self.object = form.save(commit=False)
            self.object.nodes = nodes
            self.object.save()
            return redirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form,formset=formset))


class AdminSectionListView(LoginRequiredMixin, AdminListView):
    model = Section
    def get_context_data(self, **kwargs):
        context = super(AdminSectionListView,self).get_context_data(**kwargs)
        table = SectionTable(self.get_queryset())
        RequestConfig(self.request, paginate={"per_page": 20}).configure(table)
        context['section_table'] = table
        context['section_tree'] = Section.objects.get_tree()
        return context

class AdminSectionUpdateView(LoginRequiredMixin, AdminUpdateView):
    model = Section
    form_class = SectionForm
