"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from django.test import TestCase
from articles.models import Article
from taxonomy.models import Section

class AdminAuthTestCase(TestCase):
    def test_index(self):
        resp = self.client.get(reverse('admin:index'))
        self.assertEqual(resp.status_code, 302)

class AdminArticlesTestCase(TestCase):
    def setUp(self):
        self.section = Section.objects.create(name='test',slug='test')
        self.article = Article.objects.create(title='A test article',primary_section=self.section)
        self.user = User.objects.create(username='testuser')
        self.user.set_password('password')
        self.user.save()
    def test_delete_button(self):
        response = self.client.login(username='testuser',password='password')
        self.assertEqual(response,True)
        resp = self.client.post(reverse('admin:articles_article_change',args=[self.article.id]),data={'wf_target':'T'})
        self.assertEqual(resp.status_code,200)
        self.assertEqual(self.article.pub_status,'T')

