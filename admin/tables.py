from pprint import pprint
from bs4 import BeautifulSoup
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
import django_tables2 as tables
from django_tables2.utils import A, Attrs
from sorl import thumbnail

class ArticleTable(tables.Table):
    id = tables.Column()
    title = tables.LinkColumn('admin:articles_article_change',kwargs={'pk':A('pk')})
    created_by = tables.Column()
    author = tables.Column()
    primary_section = tables.Column()
    pub_status = tables.Column()
    pub_date = tables.Column(verbose_name='Publish date')
    modified = tables.Column()

    def render_primary_section(self,record):
        return record.primary_section_id

    def render_author(self,value):
        authors = [a.name for a in value.all()]
        return ', '.join(authors)

    def render_pub_status(self,record):
        return record.get_pub_status_desc()

    def render_pub_date(self,record):
        if record.is_published:
            return record.pub_date
        return ''

    class Meta:
        attrs = {'class': 'palegrey'}

class ImageTable(tables.Table):
    thumbnail = tables.Column(verbose_name='',accessor='file')
    description = tables.Column()
    credit = tables.Column()
    created_on = tables.Column()

    def render_thumbnail(self,record):
        thumbnail = record.get_thumbnail('70x70', crop='center')
        attrs = {
            'src': thumbnail.url,
            'height': thumbnail.height,
            'width': thumbnail.width
        }
        soup = BeautifulSoup()
        img = soup.new_tag('img',**attrs)
        link = soup.new_tag('a',href=reverse('admin:images_image_edit',args=[record.id]))
        link.append(img)
        return mark_safe(link)

    class Meta:
        attrs = {'class': 'palegrey'}


class AuthorTable(tables.Table):
    thumbnail = tables.Column(verbose_name='',accessor='image')
    name = tables.LinkColumn('admin:articles_author_change', args=[A('pk')])
    position = tables.Column()

    class Meta:
        attrs = {'class': 'palegrey'}

    def render_thumbnail(self,record):
        if not record.image:
            return ''
        thumb = thumbnail.get_thumbnail(record.image,'70x70',crop='center')
        attrs = {
            'src': thumb.url,
            'height': thumb.height,
            'width': thumb.width
        }
        soup = BeautifulSoup()
        img = soup.new_tag('img',**attrs)
        link = soup.new_tag('a',href=reverse('admin:articles_author_change',args=[record.id]))
        link.append(img)
        return mark_safe(link)

class SectionTable(tables.Table):
    name = tables.Column()
    path = tables.Column()
    class Meta:
        attrs = {'class': 'palegrey section-list'}

    def render_name(self,record):
        soup = BeautifulSoup()
        soup = BeautifulSoup()
        link = soup.new_tag('a',href=reverse('admin:taxonomy_section_edit',args=[record.id]))
        link.append(record.name)
        return mark_safe("%s %s" % ('-'*record.level,link))


class ArticleListTable(tables.Table):
    title = tables.LinkColumn('admin:layout_articlelist_change',args=[A('pk')])
    slug = tables.Column()
    list_items = tables.Column(verbose_name='No. of items')

    def render_list_items(self,record):
        return str(len(record.nodes))

    class Meta:
        attrs = {'class': 'palegrey'}