from django.conf.urls.defaults import patterns, url
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView, CreateView
from django.views.generic.list import ListView
from admin.forms.article import AdminArticleForm, AdminAuthorForm
from admin.forms.layout_forms import AdminWellForm, AdminArticleListCreateForm, AdminArticleListForm
from admin.forms.taxonomy_forms import SectionForm
from admin.views import AdminArticleListView, AdminImageListView, AdminWellUpdateView, AdminListView, AdminCreateView, AdminArticleCreateView, AdminImageUpdateView, AdminArticleUpdateView, AdminWellCreateView, AdminAuthorListView, AdminUpdateView, AdminArticleDeleteView, AdminImageCreateView, AdminSectionListView, AdminSectionUpdateView, AdminArticleListListView, AdminArticleListUpdateView
from articles.models import Article, ArticleImage, Author
from images.models import Image
from layout.models import Well, ArticleList
from taxonomy.models import Section

urlpatterns = patterns(
    '',
    url(r'^$', 'admin.views.admin_homepage_view', name='index'),

    # ARTICLES
    url(r'^article/list$', AdminArticleListView.as_view(), name='articles_article_changelist'),
    url(r'^article/edit/(?P<pk>[a-z\d]+)$', AdminArticleUpdateView.as_view(), name='articles_article_change'),
    url(r'^article/edit/(?P<pk>[a-z\d]+)/add_image$', 'admin.views.admin_articleimage_manage_view', name='articles_articleimage_create'),
    url(r'^article/create$', AdminArticleCreateView.as_view(), name='articles_article_create'),
    url(r'^article/delete/(?P<pk>[a-z\d]+)$', AdminArticleDeleteView.as_view(), name='articles_article_delete'),
    url(r'^article/edit/(?P<pk>[a-z\d]+)/manage_related$', 'admin.views.admin_article_related_manage_view', name='articles_article_related_manage'),

    url(r'^article/edit/(?P<pk>[a-z\d]+)/metadata$', 'admin.views.admin_article_metadata_manage_view', name='articles_article_metadata_manage'),

    url(r'^author/list$', AdminAuthorListView.as_view(), name='articles_author_changelist'),
    url(r'^author/edit/(?P<pk>[a-z\d]+)$', AdminUpdateView.as_view(model=Author,form_class=AdminAuthorForm, template_name='admin/articles_author_form.html'), name='articles_author_change'),
    url(r'^author/create$', AdminCreateView.as_view(model=Author, form_class=AdminAuthorForm, template_name='admin/articles_author_form.html'), name='articles_author_create'),

    # Article image insert browser
    url(r'^article/edit/(?P<pk>[a-z\d]+)/articleimages$', 'admin.views.admin_articleimage_insert_view', name='article_articleimage_list'),

    # IMAGES
    url(r'^image/list$', AdminImageListView.as_view(), name='images_image_changelist'),
    url(r'^image/edit/(?P<pk>[a-z\d]+)$', AdminImageUpdateView.as_view(), name='images_image_edit'),
    url(r'^image/create', AdminImageCreateView.as_view(), name='images_image_create'),

    # LAYOUT
    url(r'^layout$', TemplateView.as_view(template_name='admin/layout_index.html'), name='layout_index'),

    url(r'^layout/well/list$', AdminListView.as_view(model=Well, template_name='admin/layout_well_list.html'), name='layout_well_changelist'),
    url(r'^layout/well/edit/(?P<pk>[a-z\d]+)$', AdminWellUpdateView.as_view(), name='layout_well_change'),
    url(r'^layout/well/create$', AdminWellCreateView.as_view(), name='layout_well_create'),

    url(r'layout/articlelist/list$', AdminArticleListListView.as_view(), name='layout_articlelist_changelist'),
    url(r'layout/articlelist/create$', AdminCreateView.as_view(model=ArticleList,form_class=AdminArticleListCreateForm), name='layout_articlelist_create'),
    url(r'layout/articlelist/edit/(?P<pk>[a-z\d]+)$', AdminArticleListUpdateView.as_view(), name='layout_articlelist_change'),

    # TAXONOMY
    url(r'^section/list$', AdminSectionListView.as_view(), name='taxonomy_section_changelist'),
    url(r'^section/add$', CreateView.as_view(model=Section, template_name='admin/taxonomy_section_form.html', success_url='/ta_admin/section/list',form_class=SectionForm), name='taxonomy_section_create'),
    url(r'^section/edit/(?P<pk>[a-z\d]+)$', AdminSectionUpdateView.as_view(), name='taxonomy_section_edit'),
)