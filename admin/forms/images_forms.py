from django import forms
from sorl import thumbnail
from admin.widgets import SearchInput

class ImageForm(forms.ModelForm):
    file = thumbnail.fields.ImageFormField()

class AdminImageSearchForm(forms.Form):
    filter = forms.CharField(required=False,widget=SearchInput)