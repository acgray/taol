from django.forms.fields import DateTimeField
from django.forms.models import ModelForm
from admin.fields import JqSplitDateTimeField, TextListField
from admin.widgets import JqSplitDateTimeWidget, AutoSlugWidget, ChosenSelectWidget, TinyMCEWidget, SearchInput
from articles.models import Article, Author
from django import forms
from utils.widgets import  SplitSelectDateTimeWidget


class AdminArticleForm(ModelForm):
    def __init__(self, request_user, *args, **kwargs):
        super(AdminArticleForm,self).__init__(*args,**kwargs)
        try:
            if not request_user.has_perm('articles.publish_article'):
                self.fields.pop('pub_date')
        except AttributeError:
            return

        instance = getattr(self, 'instance', None)
        if instance and instance.is_published:
            self.fields['slug'].widget.attrs['readonly'] = True

    pub_date = JqSplitDateTimeField(required=False)
    display_main_image = forms.TypedChoiceField(
        coerce=lambda x: True if x == 'True' else False,
        choices=((False, 'No'), (True, 'Yes')),
        widget=forms.Select
    )
    keywords = TextListField(required=False)

    class Meta:
        model = Article
        fields = ['title','slug','deck','author','body','primary_section','comments_enabled','custom_template','display_main_image','main_image_caption','pub_date','keywords','topics','editors_pick','pin_on_section_page','pin_on_homepage']
        widgets = {
            'author': ChosenSelectWidget(attrs={'data-placeholder': 'Select an author...'}),
            'topics': ChosenSelectWidget(attrs={'data-placeholder': 'Select a topic...'}),
            'slug': AutoSlugWidget,
        }

class AdminArticleCreateForm(ModelForm):
    type = forms.ChoiceField(choices=(
        ('simple','Simple'),
        ('sport','Sport'),
        ('review','Review')
    ))
    class Meta:
        model = Article
        fields = ['title']

class AdminArticleSearchForm(forms.Form):
    filter = forms.CharField(required=False,widget=SearchInput)

class AdminAuthorForm(ModelForm):
    class Meta:
        model = Author
        widgets = {
            'slug': AutoSlugWidget,
            'biog': forms.widgets.Textarea(attrs={'class':'tinymce'}),
        }
