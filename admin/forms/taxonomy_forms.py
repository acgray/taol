from django import forms
from taxonomy.models import Section

class SectionForm(forms.ModelForm):
    class Meta:
        model = Section
        exclude = ['ancestors','path']