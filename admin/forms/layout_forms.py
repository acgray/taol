from django import forms
from django.utils import simplejson
from admin.fields import JqSplitDateTimeField, TextListField
from admin.widgets import AutoSlugWidget
from layout.models import Node, Well, ArticleList
from utils.widgets import SelectTimeWidget, SplitSelectDateTimeWidget

class AdminWellForm(forms.ModelForm):
    node_data = forms.CharField(widget=forms.HiddenInput,required=False)
    pub_date = JqSplitDateTimeField()
    expires = JqSplitDateTimeField(required=False)

    def __init__(self,*args,**kwargs):
        super(AdminWellForm,self).__init__(*args,**kwargs)
        if self.initial:
            ids = []
            for n in self.instance.nodes:
                ids.append(n.content_object.id)
            self.initial['node_data'] = simplejson.dumps(ids)

    def save(self, *args, **kwargs):
        nodes = []
        if self.cleaned_data.get('node_data'):
            ids= simplejson.loads(self.cleaned_data.get('node_data'))
            for id in ids:
                nodes.append(Node(content_object_id=id))
        self.instance.nodes = nodes
        super(AdminWellForm,self).save(*args,**kwargs)


    class Meta:
        model = Well
        exclude=['nodes']
        widgets={'expires': SplitSelectDateTimeWidget()}

class AdminWellCreateForm(forms.ModelForm):
    class Meta:
        model = Well
        fields = ['type']

    def save(self, *args, **kwargs):
        print 'saving'
        super(AdminWellCreateForm,self).save(*args, **kwargs)

class AdminArticleListCreateForm(forms.ModelForm):
    class Meta:
        model = ArticleList
        fields = ['title','slug']
        widgets = {
            'slug': AutoSlugWidget,
        }


class AdminArticleListForm(forms.ModelForm):
    class Meta:
        model = ArticleList
        exclude = ['nodes']