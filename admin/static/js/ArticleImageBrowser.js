function ArticleImageBrowser(field_name, url, type, win) {
    alert("Field_Name: " + field_name + "\nURL: " + url + "\nType: " + type + "\nWin: " + win);
    if (type=='image') {
        var browserUrl = window.location + '/articleimages'
    } else {
        alert('Not implemented: link browser');
        return;
    }
    tinyMCE.activeEditor.windowManager.open({
        file: browserUrl,
        title: 'Article Images',
        width: 500,
        height: 400,
        close_previous: "no",
        resizeable: "yes",
        inline: "yes",
    }, {
        window: win,
        input: field_name,
    })
}