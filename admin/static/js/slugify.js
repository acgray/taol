function slugify(Text)
{
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}
$(function() {
    $('#id_name').bind('input keyup paste', function() {
        $('#id_slug').val(slugify(this.value))
    })
});

$(function() {
    $('#id_title').bind('input keyup paste', function() {
        if (this.value.length < 101) {
            $('#id_slug').val(slugify(this.value))
        }
    })
});