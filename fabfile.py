from fabric.context_managers import cd
from fabric.operations import run, sudo
from fabric.state import env

env.hosts = ['adam@server.theanglenews.co.uk:1022']
env.project_root = '/srv/webapps/theangle/taol'
env.activate_script = '/srv/webapps/.virtualenvs/taol/bin/activate'

def virtualenv(command, use_sudo=False):
    if use_sudo:
        func=sudo
    else:
        func=run

    func('source "%s" && %s' %(env.activate_script, command))

def manage_py(command, use_sudo=False):
    with cd(env.project_root):
        virtualenv('python manage.py %s --settings=taol.settings.production' % command, use_sudo)

def deploy():
    with cd(env.project_root):
        sudo('git pull')
        manage_py('collectstatic -l --noinput',True)
        sudo('service taol restart')

def deploy_files():
    with cd(env.project_root):
        sudo('git pull')