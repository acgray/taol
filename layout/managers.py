import datetime
from django.db import models
from django.db.models.query_utils import Q
from articles.helpers.publication import PUB_STATUSES, PublishedManager

class WellManager(PublishedManager):
    def get_current(self, title):
        now = datetime.datetime.now()
        queryset = self.filter(type=title).filter(Q(expires__gt=now)|Q(expires__isnull=True))
        #TODO: optimize this query
        #queryset = self.filter(type=title, pub_status='P', pub_date__lte=now)#.exclude(expires__isnull=False)
        # don't need pub_date__lte=now as inheriting from PublishedManager

        try:
            return queryset[0]
        except IndexError:
            raise self.model.DoesNotExist
            pass