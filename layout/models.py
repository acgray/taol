import datetime
from django.db import models

# Create your models here.
from django.db.models import Model
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from django_mongodb_engine.contrib import MongoDBManager
from djangotoolbox.fields import ListField, EmbeddedModelField
from articles.helpers.publication import PublicationMixin
from layout.managers import WellManager
from articles.models import Article



class WellType(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)

    def __unicode__(self):
        return self.title


class Node(models.Model):
    content_object = models.ForeignKey('articles.Article', related_name='nodes', null=True)
    title_override = models.CharField(max_length=255,blank=True,null=True)

    def __unicode__(self):
        return "Node: %s" % self.title

    @property
    def is_published(self):
        return self.content_object.is_published == True

    @property
    def title(self):
        return self.title_override or self.content_object.title

    class Meta:
        abstract = True


class BaseArticleList(Model):
    class Meta:
        abstract=True
    def clean_nodes(self):
        #check all nodes still exist
        nodes = list(self.nodes)
        for k,n in enumerate(nodes):
            try:
                Article.objects.get(id=n.content_object_id)
            except Article.DoesNotExist:
                nodes.remove(n)
        self.nodes = nodes
        self.save()

    @property
    def items(self):
        content_list = []
        for n in self.nodes:
            if n.is_published:
                content_list.append(n.content_object)
        try:
            if self.queryset:
                for i in content_list:
                    self.queryset = self.queryset.exclude(id=i.id)
                from itertools import chain
                return list(chain(content_list,self.queryset))
            else:
                return content_list
        except AttributeError:
            return content_list


class Well(PublicationMixin, BaseArticleList):
    """
    Time-sensitive lists of content tied to a page layout template
    """
    type = models.ForeignKey(WellType,to_field='slug')
    expires = models.DateTimeField(null=True,blank=True)
    nodes = ListField(EmbeddedModelField(Node))

    objects = MongoDBManager()
    published = WellManager()

    def __init__(self,*args,**kwargs):
        super(Well, self).__init__(*args, **kwargs)
        self.queryset = None




    @property
    def title(self):
        return self.type.title

    def __unicode__(self):
        return "%s (%s - %s)" % (self.type, self.pub_date, self.expires or "")

    def save(self, *args, **kwargs):
        if not self.pub_date:
            self.pub_date = datetime.datetime.now()
        return super(Well, self).save(*args, **kwargs)



    def merge_with(self, queryset):
        self.queryset = queryset
        return self.items

# delete well nodes on article delete
@receiver(pre_delete, sender=Article)
def stale_node_delete(sender, **kwargs):
    wells = Well.objects.raw_query({'nodes.content_object_id':kwargs['instance'].id})
    for w in wells:
        w.nodes = filter(lambda x: x != kwargs['instance'].id, w.nodes)
        w.save()

@receiver(pre_delete, sender=Article)
def stale_node_delete(sender, **kwargs):
    lists = ArticleList.objects.raw_query({'nodes.content_object_id':kwargs['instance'].id})
    for l in lists:
        l.nodes = filter(lambda x: x != kwargs['instance'].id, l.nodes)
        l.save()

class ArticleList(BaseArticleList):
    title = models.CharField(max_length=255)
    slug = models.SlugField()
    objects = MongoDBManager()
    nodes = ListField(EmbeddedModelField(Node))
    def __unicode__(self):
        return self.title