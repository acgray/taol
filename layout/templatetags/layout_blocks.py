from layout.models import ArticleList
from layout.utils import render_model

__author__ = 'acgray'


from django import template

register = template.Library()

class ArticleListBlockNode(template.Node):
    def __init__(self,slug,name):
        try:
            self.list = ArticleList.objects.get(slug=slug)
        except ArticleList.DoesNotExist:
            self.list = None
        self.name = template.Variable(name)

    def render(self,context):
        if not self.list:
            return ''
        name = self.name.resolve(context)
        return render_model(self.list,name,context_instance=context)

@register.tag
def article_list_block(parser,token):
    try:
        _, slug, name = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r takes exactly 2 arguments")

    return ArticleListBlockNode(slug,name)

class ArticleListNode(template.Node):
    def __init__(self,slug,context_var):
        try:
            self.list = ArticleList.objects.get(slug=slug)
        except ArticleList.DoesNotExist:
            self.list = None
        self.context_var = context_var

    def render(self,context):
        context[self.context_var] = self.list
        return ''

@register.tag
def load_article_list(parser,token):
    try:
        _, slug, _, context_var = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("syntax: load_article_list slug as var")

    return ArticleListNode(slug,context_var)