from django.conf.urls.defaults import patterns, url
from articles.models import Article
from layout.views import QuerySetBackedWellView, SimpleWellView

urlpatterns = patterns('',
    #url(r'^well_test$', QuerySetBackedWellView.as_view(well_title='test-well-type', template_name='wells/homepage.html', queryset=Article.published.all()), name='well_test')
    url(r'^well_test$', SimpleWellView.as_view(well_title='test-well-type', template_name='wells/homepage.html'), name='well_test')
)