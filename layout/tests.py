"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
import datetime
from django.core.urlresolvers import reverse

from django.test import TestCase
from articles.helpers.publication import PUB_STATUSES
from articles.models import Article
from layout.models import Well, WellType, Node

class NodeTestCase(TestCase):

    def test_title_override(self):
        article = Article(title='Test',slug='test',pub_status=PUB_STATUSES['Published'])
        node = Node(content_object=article,title_override='Title override')
        self.assertEqual(node.title, 'Title override')

    def test_is_published(self):
        article = Article(title='Test',slug='test',pub_status=PUB_STATUSES['Published'])
        node = Node(content_object=article)
        self.assertTrue(node.is_published)
        article.pub_status=PUB_STATUSES['Draft']
        node = Node(content_object=article)
        self.assertFalse(node.is_published)


class WellModelTestCase(TestCase):

    urls = 'layout.test_urls'

    def test_title(self):
        wt = WellType(title='Test',slug='test')
        well = Well(type=wt,pub_status=PUB_STATUSES['Published'],pub_date=datetime.datetime(2012,8,1))
        self.assertEqual(well.title,wt.title)

    def test_items_simple_len(self):
        wt = WellType.objects.create(
            title='Test well type',
            slug='test-well-type'
        )
        well = Well.objects.create(
            type=wt,
            pub_status=PUB_STATUSES['Published'],
            pub_date=datetime.datetime(2012, 8, 1)
        )
        well.nodes = []
        for n in range(1,10):
            a = Article.objects.create(
                title='My test article'+str(n),
                slug='my-test-article-'+str(n),
                pub_status = PUB_STATUSES['Draft']
            )
            if n % 2 == 0:
                a.pub_status = PUB_STATUSES['Published']
                a.save()
            well.nodes.append(Node(content_object=a))

        well.save()

        #only 4 published items
        self.assertEqual(len(well.items), 4)

    def test_items_queryset_published(self):
        wt = WellType(title='Test',slug='test')
        w = Well(type=wt,pub_status='P',pub_date=datetime.datetime(2012,8,1))
        w.merge_with(Article.published.all())
        for i in w.items:
            self.assertTrue(i.is_published)

    def test_simplewellview_context(self):
        wt = WellType.objects.create(title='Test',slug='test-well-type')
        well = Well.objects.create(type=wt,pub_status='P')
        resp = self.client.get(reverse('well_test'))
        self.assertEqual(resp.status_code,200)

    def test_stale_node_delete_signal(self):
        article_1 = Article.objects.create(title='Stale node deletion test article', slug='stale-node-deletion-test-article')
        article_2 = Article.objects.create(title='Stale node deletion test article 2', slug='stale-node-deletion-test-article-2')
        wt = WellType(title='Test',slug='test')

        well = Well.objects.create(
            type=wt,
            nodes=[
                Node(content_object=article_1),
                Node(content_object=article_2)
            ]
        )

        # deleted article should not appear in well nodes
        article_1.delete()

        for n in well.nodes:
            self.assertNotEqual(n.content_object_id, article_1.id)


class WellManagerTestCase(TestCase):
    urls = 'layout.test_urls'
    def test_get_current(self):
        wt = WellType.objects.create(
            title = 'Test well type',
            slug = 'test-well-type'
        )
        well = Well.objects.create(
            type=wt,
            pub_date=datetime.datetime(2012,8,1),
            pub_status=PUB_STATUSES['Published'],
            expires=datetime.datetime.now()+datetime.timedelta(days=1) # expires tomorrow
        )
        self.assertEqual(Well.published.get_current('test-well-type').id,well.id)