/**
 * Created with PyCharm.
 * User: acgray
 * Date: 26/07/2012
 * Time: 21:47
 * To change this template use File | Settings | File Templates.
 */

article = {
    title: "My Test Article",
    deck: "Lorem ipsum dolor sit amet",
    body: "Full body text here",
    author_id: [
        "89s0a8d0d9f80e80a",
        ""
    ],
    author_extra: "additional reporting by Joe",
    images: [
        {
            image_id: "48728a89d0ab08e9f",
            main_image: true,
            format_overrides: {
                '600x400': ['x', 'y'],
                format_name2: ['x', 'y']
            }
        }
    ],
    metadata: {
        review: {
            rating: '5',
            title: 'The Quick Brown Fox',
            date: date(2012,8,1),
        },
        sportresult: {
            team1_name: "UCL Rangers",
            team1_result: 1,
            team2_name: "KCL",
            team2_result: 5
        }
    }
};

image = {
    file: ":-)",
    photographer: "Adam Gray",
    date_taken: date('2012','09',1),
    date_uploaded: date(2012,8,1),
};

imageformat = {
    name: "600x400",
    height: 600,
    width: 400
};

author = {
    name: "Adam Gray",
    title: "Senior sports correspondent",
    biog: "Lorem ipsum dolor sit amet",
    photo: ":-)",
};


section = {
    name: "News",
    slug: "news"
};

// LAYOUT

welltype = {
    name: 'Front page',
    slug: 'front_page'
};

well = {
    type: 'front_page',
    nodes: [
        { content_item_id: 'a8eba8c99ea09a8c89',
            title_override: 'New title',
            teaser_override: 'Adam checks out whats going on...'},
        { content_item_id: 'a8eba8c99ea09a8c89',
            title_override: 'New title',
            teaser_override: 'Adam checks out whats going on...'},
    ],
    active: 1, //Bool, default: 0
    expires: '2012-12-04 00:00:00' //blank: True, null: True
};