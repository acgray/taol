from bs4 import BeautifulSoup
from django import template
from django.core.urlresolvers import reverse, NoReverseMatch
from django.template.base import TemplateSyntaxError
from taxonomy.models import Section

__author__ = 'acgray'

register = template.Library()

class BaseSectionNode(template.Node):
    def __init__(self,slug):
        try:
            self.section = Section.objects.get(slug=slug)
        except Section.DoesNotExist:
            self.section = None

class SectionMenuLinkNode(BaseSectionNode):

    def render(self,context):
        if not self.section:
            return ''

        soup = BeautifulSoup()

        args = {
            'href': reverse('section_detail',kwargs={'slug':self.section.slug}),
            'class': self.section.slug,
        }
        if context.get('current_section') == self.section.slug:
            args['class'] += ' current'
        link = soup.new_tag('a',**args)
        link.append(soup.new_string(self.section.name))
        return link


@register.tag()
def section_menu_link(parser, token):
    try:
        tag_name, slug = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires a single argument" % token.contents.split()[0])
    return SectionMenuLinkNode(slug)

class RelatedNode(template.Node):
    def __init__(self, object_list, viewname):
        self.object_list = template.Variable(object_list)
        self.viewname = viewname

    def render(self, context):
        def node(object):
            try:
                reversed = reverse(self.viewname, args=[object.id])
            except NoReverseMatch:
                return '<li>%s</li>' % object

            return '<li><a href="%s">%s</li>' % (reversed, object)

        def recursive(object):
            if object.children.count():
                output.append('<ul>')
                for object in object.children.all():
                    output.append(node(object))
                    recursive(object)
                output.append('</ul>')

        output = []
        for object in self.object_list.resolve(context):
            if not isinstance(object, list):
                output.append(node(object))
                recursive(object)

        return '\n'.join(output)

@register.tag
def related_linked_list(parser, token):
    bits = token.contents.split()
    if len(bits) != 3:
        raise TemplateSyntaxError, "'%s' tag takes exactly 2 arguments" % bits[0]
    return RelatedNode(bits[1], bits[2])
