from django.db import models
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from django.template.defaultfilters import slugify
from django_mongodb_engine.contrib import MongoDBManager
from djangotoolbox.fields import ListField, EmbeddedModelField, DictField
from mongom2m.fields import MongoDBManyToManyField

class SectionManager(MongoDBManager):
    def get_tree(self):
        tree = []
        for c in self.get_top_level_items():
            tree += c.as_list()
        return tree

    def get_top_level_items(self):
        return self.raw_query({'ancestors':[]}).order_by('order')


class Section(models.Model):
    name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(unique=True)
    #default_well_type = models.ForeignKey(to='layout.WellType',to_field='slug',blank=True,null=True)
    #default_well_type = models.ForeignKey('layout.WellType')
    parent = models.ForeignKey('self',blank=True,null=True,related_name='children')
    ancestors = MongoDBManyToManyField('self',embed=True,related_name='descendants',blank=True,null=True)
    path = models.CharField(max_length=255)
    order = models.IntegerField(blank=True,null=True)
    objects = SectionManager()

    def __unicode__(self):
        return unicode(self.name)

    def save(self, force_insert=False, force_update=False, using=None):
        anc = []
        if self.parent:
            if len(self.parent.ancestors.all()):
                anc += list(self.parent.ancestors.all())
            anc.insert(0,self.parent)
        self.ancestors = anc
        path = [a.slug for a in anc]
        path.reverse()
        path.append(self.slug)
        self.path = '/'.join(path)
        return super(Section,self).save(force_insert, force_update, using)

    def get_children_list(self):
        return list(self.children.all())

    @property
    def level(self):
        return self.ancestors.count()

    def as_list(self):
        list = [self]
        if self.children.count():
            children = []
            for c in self.children.all():
                children += c.as_list()
            list.append(children)
        return list

class Topic(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField()

    def __unicode__(self):
        return self.name

    def save(self, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
            return super(Topic,self).save()
