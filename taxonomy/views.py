# Create your views here.
from django.views.generic.list import MultipleObjectMixin
from articles.models import Article
from layout.models import Well, WellType
from layout.views import QuerySetBackedWellView, SimpleWellView
from taxonomy.models import Section

class DefaultSectionView(QuerySetBackedWellView):
    template_name = 'layout/wells/default.html'
    well_title = 'default'

    def get(self, request, *args, **kwargs):
        self.section = Section.objects.get(slug=kwargs.get('slug'))
        self.queryset = self.get_queryset(**kwargs)
        return super(DefaultSectionView,self).get(request, *args, **kwargs)

    def get_well(self):
        well = Well(type=WellType(title='Default',slug='default'))
        well.merge_with(self.queryset)
        return well

    def get_queryset(self, **kwargs):
        qs = Article.published.filter(primary_section=self.section).order_by('-pin_on_section_page','-pub_date')
        return self.queryset or qs

    def get_context_data(self, **kwargs):
        context = super(DefaultSectionView,self).get_context_data(**kwargs)
        context['current_section'] = kwargs.get('slug')
        context['section'] = self.section
        context['page_title'] = self.section.name
        return context

class NewsSectionView(DefaultSectionView):
    well_title = 'news'
    template_name = 'layout/wells/news.html'