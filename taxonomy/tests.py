"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from taxonomy.models import Section

class SectionTestCase(TestCase):
    def setUp(self):
        self.parent = Section.objects.create(name='Parent section',slug='parent')
        self.child = Section.objects.create(name='Child section', slug='child',parent=self.parent)
        self.child2 = Section.objects.create(name='Sister section', slug='child2',parent=self.parent)
        self.grandchild = Section.objects.create(name='Grandchild section', slug='grandchild',parent=self.child)


    def test_get_tree(self):
        print self.parent.as_list()