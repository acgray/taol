from django.core import files
import factory
import os
from models import Image
from django.contrib.webdesign import lorem_ipsum as lipsum
from PIL import Image as PILImage, ImageDraw
from taol.settings import MEDIA_ROOT

def dummy_image(n):
    im = PILImage.new('RGB', (800,600), (200,200,200,0))
    d_im = ImageDraw.Draw(im)
    d_im.text((300,300),str(n),(0,0,0))
    fn = os.path.join(MEDIA_ROOT,'test','test'+str(n)+'.jpg')
    im.save(fn, 'JPEG')
    return files.File(open(fn), 'test/test'+str(n)+'.jpg')

class ImageFactory(factory.Factory):
    FACTORY_FOR = Image

    description = lipsum.words(5)
    credit = lipsum.words(2)
    file = factory.Sequence(lambda n: dummy_image(n))