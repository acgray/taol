from django.contrib import admin
from sorl.thumbnail.admin.current import AdminImageMixin
from images.models import Image

class ImageAdmin(AdminImageMixin, admin.ModelAdmin):
    model = Image
    pass

admin.site.register(Image, ImageAdmin)