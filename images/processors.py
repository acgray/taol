from PIL import Image

def cropbox(im,requested_size,opts):
    print opts
    if "cropbox" in opts:
        im.crop(cropbox)
        im.resize(requested_size)
        return im

cropbox.valid_options = ("cropbox",)