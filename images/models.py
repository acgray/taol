import datetime
from django.core.urlresolvers import reverse
from django.db import models
from djangotoolbox.fields import ListField
from sorl import thumbnail

class Image(models.Model):
    file = thumbnail.ImageField(upload_to='images') # TODO: change upload directory
    description = models.CharField(max_length=255)
    credit = models.CharField(max_length=255,blank=True,null=True)
    created_on = models.DateTimeField(auto_now=True)

    def get_thumbnail(self,geometry_string,**kwargs):
        return thumbnail.get_thumbnail(self.file,geometry_string,**kwargs)

    class Meta:
        ordering = ['-created_on']
        get_latest_by = 'created_on'

class ImageFormat(models.Model):
    name = models.SlugField()
    height = models.IntegerField()
    width = models.IntegerField()
